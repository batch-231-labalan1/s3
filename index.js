console.log('hi');

//To create a class in JS, we us the class keyword and {}.
//naming convention for classes begins with uppercase characters

/*
	syntax:
	class ClassNAme {
		properties
	};
*/

class Dog {
	//we add constructor method to a class to be able to initialize values upon instantiation of an object from a class.
	constructor(name, breed, age){

		this.name = name;
		this.breed = breed;
		this.age = age * 7;

	}
}

// instantiation - process of creating objects
// an object created from class is called an instance
// new keyword is used to instantiate an object from a class

let dog1 = new Dog("Puchi", "golden retriever", 0.6);
console.log(dog1);




/*

===============Function Coding Activity: (45 mins) ==============

1. Modify the class Person and add the following methods:
    a. greet method- the person should be able to greet a certain message
    b. introduce method- person should be be able to state their own name
    c. change address- person should be able to update their address and send a message where he now lives
2. All methods should be chainable. 
3. Refactor the age by having a condition in which the age should only take in a number data type and a legal age of 18, otherwise undefined


*/


class Person {
	constructor ( name, age, nationality, address){

		this.name = name;
		this.age = age;
		this.nationality = nationality;
		this.address = address;
		
		if (typeof age === "number" && age >= 18 ) {
            this.age = age;
        } else {
            this.age = undefined
        }


	}

	greet(){
		console.log("Hello! Good Morning");
		return this;
	}

	introduce(){
		console.log(`Hi! My name is ${this.name}`);
		return this;

	}

	changeAddress(newAddress){
			this.address = newAddress;
			console.log(`${this.name} now lives in ${this.address}`)
			return this;
		}

}

let person1 = new Person( "Ace", 18, "Filipino", "Manila, Philippines");
let person2 = new Person("Mist", 17, "Japanese", "Tokyo, Japan");

console.log(person1);
console.log(person2);


/*
======== MINI QUIZ ========

1. What is the blueprint where objects are created from?
        Answer: class/classes


    2. What is the naming convention applied to classes?
        Answer: Uppercase characters


    3. What keyword do we use to create objects from a class?
        Answer: new


    4. What is the technical term for creating an object from a class?
        Answer: instantiation


    5. What class method dictates HOW objects will be created from that class? 
    	Answer: constructor

*/



// ============= CLASS STUDENT ================

class Student {

	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		// this.grades = grades;

		//check first if the array has 4 elements
		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <=100)){
				this.grades = grades;
			}else {
				this.grades = undefined;
			}
		} else {
			this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;
	}


	//make sure all methods in class are not separated by comma
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}

	logout(){
		console.log(`${this.email} has logged out`);
		return this;
	}

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		})
		return this;
	}

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum / 4;
		return this;
	}


	willPass(){
		//you can call methods inside an object
		this.passed = this.gradeAve >= 85 ? true : false;
		return this;
	}


	willPassWithHonors(){

		if(this.passed){
			if(this.gradeAve >= 90){
				this.passedWithHonors = true;
			} else {
				this.passedWithHonors = false;
			}
		} else {
			this.passedWithHonors = false;
		}
		return this;
	}

}



//instantiate all fours students from s2 using student class

let studentOne =  new Student("Tony", "starkindustries@gmail.com", [89, 84, 78, 88]);
let studentTwo =  new Student("Peter", "spideyman@gmail.com", [78, 82, 79, 85]);
let studentThree =  new Student("Wanda", "scarlettMaximoff@gmail.com", [87, 89, 91, 93]);
let studentFour =  new Student("Steve", "captainRogers@gmail.com", [91, 89, 92, 93]);



//Chain of METHODS
// studentOne.login().listGrades().logout()
studentOne.computeAve().willPass().willPassWithHonors()




// ============= ACTIVITY #3 ===============

/*

     1. Should class methods be included in the class constructor?
        Answer: no


    2. Can class methods be separated by commas?
        Answer: no


    3. Can we update an object’s properties via dot notation?
        Answer: yes


    4. What do you call the methods used to regulate access to an object’s properties?
        Answer:  class methods


    5. What does a method need to return in order for it to be chainable?
        Answer: this

*/

















